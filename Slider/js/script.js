const upBtn = document.querySelector('.up-button');
const dounBtn = document.querySelector('.doun-button');
const conteiner = document.querySelector('.conteiner');
const sidebar = document.querySelector('.sidebar');
const mainSlide = document.querySelector('.main-slide');
const slidesCound =mainSlide.querySelectorAll('div').length;

let activeSlideIndex = 0;
 // So that the slider is inserted to the entire screen size
 sidebar.style.top = `-${(slidesCound - 1) * 100}vh`;
// To make the slider switch by the up and down buttons
 upBtn.addEventListener('click', () => {
     changeSlide('up');
 })
 dounBtn.addEventListener('click', () => {
    changeSlide('down');
 })
// The main function for slider operation. moving from one to the second slider
 function changeSlide(direction) {
    if (direction === 'up') {
        activeSlideIndex++;
        if (activeSlideIndex === slidesCound)   {
            activeSlideIndex = 0;
        }
    }else if (direction === 'down') {
        activeSlideIndex--;
        if (activeSlideIndex < 0) {
            activeSlideIndex = slidesCound - 1;
        }
    }
 // Animation Work
    const height = conteiner.clientHeight;
    mainSlide.style.transform = `translateY(-${activeSlideIndex * height}px)`;
    sidebar.style.transform = `translateY(${activeSlideIndex * height}px)`;
 }





